# Slim Framework Fluid View

This is a Slim Framework view helper built on top of the TYPO3 Fluid templating engine.
You can use this component to create and render templates in your Slim Framework application.


## Install

Via Composer

```
$ composer require slim-view/fluid
```

Requires Slim Framework 3 and PHP 5.5 or newer. 


## Usage

```php
<?php
// Create Slim app
$app = new \Slim\App();

// Fetch DI Container
$container = $app->getContainer();

// Register Fluid View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Fluid([
        'templateRootPaths' => ['path/to/Templates/'],
        'partialRootPaths' => ['path/to/Partials/'],
        'layoutRootPaths' => ['path/to/Layouts/'],

        'singlePath' => 'path/to/Singles/',
        'cachePath' => 'path/to/fluid_cache/',
        
        'viewHelperNamespaces' => [
            'vh' => 'VendorName\\Package\\ViewHelper'
        ],
    ]);
    return $view;
};

// Renders template file "path/to/Singles/Profile.html"
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $this->view->render($response, 'Profile', [
        'name' => $args['name']
    ]);
})->setName('profile');

// Run app
$app->run();

```

If you've outsourced your route logics to controllers and actions, your action may look like this,
assuming that `$this->view` is instance of `\Slim\Views\Fluid`:

```php
<?php
class ProfileController
{
    public function logoutAction($request, $response, $args)
    {
        $this->view->setController('Profile');
        $this->view->setAction('logout');
        $this->view->assign('name', $args['name']);
        
        // Template file loaded: path/to/Templates/Profile/Logout.html
        return $this->view->render($response);
    } 
}
```

In case you want to render fluid templates without involving the response, you can use the `fetch()` method:

```php
<?php
$app->get('/example/{name}', function ($request, $response, $args) {
    $renderedContent = $this->view->fetch('Profile', [
        'name' => $args['name']
    ]);
    // ...
});
```

### Register own view helper namespace

To register a viewhelper namespace in fluid, just add an entry to `viewHelperNamespaces` configuration:
For example:

```
[
    'abc' => 'Vendor\\Package\\ViewHelper',
]
```

A hello world view helper looks like this:
```php
<?php
namespace Vendor\Package\ViewHelper;

/**
 *
 * @package Vendor\Package\ViewHelper
 */
class HelloWorldViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    public function render()
    {
        return 'Hello World!';
    }
}
```

And you call it in fluid template, this way:
```html
<abc:helloWorld />
```



## Fluid Templates

Basically a fluid template may look like this:

```html
<span>Hello {name}!</span>
```

Fluid ships some basic view helpers, you may use:
```html
<span>
    <f:if condition="{name}">
        <f:then>Hello {name}!</f:then>
        <f:else>Hello stranger!</f:else>
    </f:if>
</span>
```

There is also an **inline notation** existing for calling view helpers:
```html
<span>
    Hello {f:if(condition:'{name}', then: '{name}', else: 'Stranger')}!
</span>
```

A view helper is basically a php class. The fluid default view helpers are prefixed with `f:`.
You may introduce own namespaces for your custom view helpers.


### Further fluid documentation

Code is the best documentation. Unfortunately I did not find a full compendium of standalone fluid,
because originally this template engine were part of TYPO3 CMS.

However, the fluid package ships some examples. Here you get an [overview of how layouts and partials work][1].
The shipped view helper classes can be found [here][2]. Each has an usage example in comments.

Also checkout the [README][3] of fluid, to understand the concepts and learn how to write own view helpers.



[1]: https://github.com/TYPO3/Fluid/tree/master/examples/Resources/Private
[2]: https://github.com/TYPO3/Fluid/tree/master/src/ViewHelpers
[3]: https://github.com/TYPO3/Fluid
