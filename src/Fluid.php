<?php
namespace Slim\Views;

use Psr\Http\Message\ResponseInterface;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

class Fluid
{
    /**
     * @var \TYPO3Fluid\Fluid\View\TemplateView
     */
    protected $instance;

    /**
     * @var string
     */
    protected $singlePathPrefix = '';

    /**
     * Create new Fluid view
     *
     * @param array $settings
     * @param RenderingContextInterface|null $context
     */
    public function __construct(array $settings = [], RenderingContextInterface $context = null)
    {
        $this->instance = new \TYPO3Fluid\Fluid\View\TemplateView($context);
        // Set paths
        if (isset($settings['templateRootPaths'])) {
            $this->instance->getTemplatePaths()->setTemplateRootPaths($settings['templateRootPaths']);
        }
        if (isset($settings['partialRootPaths'])) {
            $this->instance->getTemplatePaths()->setPartialRootPaths($settings['partialRootPaths']);
        }
        if (isset($settings['layoutRootPaths'])) {
            $this->instance->getTemplatePaths()->setLayoutRootPaths($settings['layoutRootPaths']);
        }
        // Set cache
        if (isset($settings['cachePath'])) {
            if (!file_exists($settings['cachePath'])) {
                mkdir($settings['cachePath'], 0777, true);
            }
            if (is_writable($settings['cachePath'])) {
                $this->instance->setCache(new \TYPO3Fluid\Fluid\Core\Cache\SimpleFileCache($settings['cachePath']));
            }
        }
        // Register viewhelper namespaces
        if (isset($settings['viewHelperNamespaces']) && !empty($settings['viewHelperNamespaces'])) {
            foreach ($settings['viewHelperNamespaces'] as $identifier => $phpNamespace) {
                $this->instance->getViewHelperResolver()->addNamespace($identifier, $phpNamespace);
            }
        }

        // Set prefix path for single file templates (not loaded via controller context)
        if (isset($settings['singlePath'])) {
            $this->singlePathPrefix = $settings['singlePath'];
        }
    }

    /**
     * Magic __call method to access fluid template view methods
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        if (method_exists($this->instance, $name)) {
            return call_user_func_array([$this->instance, $name], $arguments);
        }
    }

    /**
     * Returns view helper resolver, to allow you to add new viewhelper namespaces
     *
     * @return \TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperResolver
     */
    public function getViewHelperResolver()
    {
        return $this->instance->getViewHelperResolver();
    }

    /**
     * Get controller name from rendering context
     *
     * @return string
     */
    public function getController()
    {
        return $this->instance->getRenderingContext()->getControllerName();
    }

    /**
     * Set controller name in rendering context
     *
     * @param string $controllerName
     */
    public function setController($controllerName)
    {
        $this->instance->getRenderingContext()->setControllerName($controllerName);
    }

    /**
     * Get action name from rendering context
     *
     * @return string
     */
    public function getAction()
    {
        return $this->instance->getRenderingContext()->getControllerAction();
    }

    /**
     * Set action name in rendering context
     *
     * @param string $actionName
     */
    public function setAction($actionName)
    {
        $this->instance->getRenderingContext()->setControllerAction($actionName);
    }

    /**
     * Get format from rendering context
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->instance->getTemplatePaths()->getFormat();
    }

    /**
     * Set format (default 'html')
     *
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->instance->getTemplatePaths()->setFormat($format);
    }

    /**
     * Returns rendered template (as string) and resolves singleTemplateName
     *
     * @param string $singleTemplateName Without file extension.
     * @param array $data
     * @return string
     */
    public function fetch($singleTemplateName = '', $data = [])
    {
        if (!empty($singleTemplateName)) {
            $path = rtrim($this->singlePathPrefix, '/') . '/' .
                trim($singleTemplateName, '/') .
                '.' . $this->getFormat();
            $this->instance->getTemplatePaths()->setTemplatePathAndFilename(realpath($path));
        }
        $this->instance->assignMultiple($data);

        return $this->instance->render();
    }

    /**
     * Output rendered template
     *
     * @param ResponseInterface $response
     * @param string $singleTemplateName
     * @param array $data
     * @return ResponseInterface
     */
    public function render(ResponseInterface $response, $singleTemplateName = '', $data = [])
    {
        $response->getBody()->write($this->fetch($singleTemplateName, $data));
        return $response;
    }
}
